<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.caballero.lv/
 * @since             1.0.0
 * @package           Cwsn
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce SMS Notifications
 * Plugin URI:        https://www.caballero.lv/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Caballero
 * Author URI:        https://www.caballero.lv/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cwsn
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CWSN_VERSION', '1.0.3' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cwsn-activator.php
 */
function activate_cwsn() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cwsn-activator.php';
	Cwsn_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cwsn-deactivator.php
 */
function deactivate_cwsn() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cwsn-deactivator.php';
	Cwsn_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_cwsn' );
register_deactivation_hook( __FILE__, 'deactivate_cwsn' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cwsn.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_cwsn() {
	if ( class_exists( 'WC_Integration' ) ) {

		$plugin = new Cwsn();
		$plugin->run();
	}
}

add_action( 'plugins_loaded', 'run_cwsn' );
