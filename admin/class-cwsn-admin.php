<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.caballero.lv/
 * @since      1.0.0
 *
 * @package    Cwsn
 * @subpackage Cwsn/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Cwsn
 * @subpackage Cwsn/admin
 * @author     Caballero <dev@caballero.lv>
 */
class Cwsn_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Adds SMS integration
	 *
	 * @param array $integrations Integrations.
	 */
	public function add_integration( $integrations ) {
		$integrations[] = 'Cwsn_Integration';
		return $integrations;
	}
}
