<?php
/**
 * Notification Integration.
 *
 * @link       https://www.caballero.lv/
 * @since      1.0.0
 *
 * @package    Cwsn
 * @subpackage Cwsn/notification
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Cwsn
 * @subpackage Cwsn/notification
 * @author     Caballero <dev@caballero.lv>
 */
class Cwsn_Notification extends Cwsn_Integration {
	/**
	 * Status changed
	 *
	 * @param string $order_id Order ID.
	 */
	public function order_status_processing( $order_id ) {
		if ( $this->enabled ) :
			$order = wc_get_order( $order_id );

			$number = $order->get_billing_phone();

			$message = $this->message;
			$message = str_replace( '{order_number}', $order->get_order_number(), $message );

			$content = ! mb_detect_encoding( $message, 'utf-8', true ) ? utf8_encode( $message ) : $message;
			$sender  = $this->sender;

			$response = $this->send_one( $number, $sender, $content );
			$response = json_decode( $response );

			if ( isset( $response->Error ) ) :
				$logger = wc_get_logger();
				$logger->debug( $response->Error, array( 'source' => 'cwsn' ) );
			endif;
		endif;
	}


	/**
	 * Sends a single message to a single recipient.
	 *
	 * @param string $number Recipient number (numbers only, with or without country code). If number doesn't contain country code,
	 *  it will be taken from the CC parameter. If that is not available as well, the default country code is assumed (on server side).
	 * @param string $sender Sender name to use for the SMS. Only assigned sender names can be used.
	 * @param string $content Message content. Must be in UTF-8 (or ASCII). All conversions are handled on server side.
	 * @param int    $send_time Message sending time (Unix timestamp). Optional, if not given, message is sent as soon as possible.
	 * @param int    $cc Country code. Optional. If country code is present in the number, that one is used even if CC is given.
	 * @param mixed  $concatenated Indicates if concatenated (multipart) messages are allowed (1/true = allowed, 0/false = not allowed, null = not relevant). Optional.
	 * @param mixed  $unicode Indicates if Unicode (shorter but full UCS-2 character set is allowed) messages are allowed.
	 *   (1/true = allowed, 0/false = not allowed, null = not relevant). Optional.
	 *
	 * @return array Server response, decoded.
	 */
	private function send_one( $number, $sender, $content, $send_time = false, $cc = false, $concatenated = null, $unicode = null ) {
		$command = array(
			'APIKey'  => $this->api_key,
			'Command' => 'SendOne',
			'Number'  => $number,
			'Sender'  => $sender,
			'Content' => $content,
		);

		if ( $send_time && is_int( $send_time ) ) {
			$command['SendTime'] = $send_time;
		}
		if ( $cc && is_int( $cc ) ) {
			$command['CC'] = $cc;
		}
		if ( ! is_null( $concatenated ) ) {
			$command['Concatenated'] = $concatenated ? 1 : 0;
		}
		if ( ! is_null( $unicode ) ) {
			$command['Unicode'] = $unicode ? 1 : 0;
		}

		return self::http_post_array( $this->url . '/API:0.12/', $command );
	}

	/**
	 * Makes a HTTP POST request
	 *
	 * @param string $url URL.
	 * @param array  $data Data.
	 * @param array  $additional_headers Additional headers, optional.
	 */
	private static function http_post_array( $url, array $data, array $additional_headers = null ) {
		if ( ! $url ) {
			return false;
		}

		$url_info = wp_parse_url( $url );

		$post_content = array();
		foreach ( $data as $key => $value ) {
			$post_content[] = $key . '=' . rawurlencode( $value );
		}
		$post_content = implode( '&', $post_content );

		$headers = array(
			'Host'           => $url_info['host'],
			'Content-Type'   => 'application/x-www-form-urlencoded',
			'Content-Length' => strlen( $post_content ),
			'Connection'     => 'close',
		);
		if ( $additional_headers ) {
			$headers = array_merge( $headers, $additional_headers );
		}
		$headers = self::header_string( $headers );

		$context = stream_context_create(
			array(
				'http' => array(
					'method'           => 'POST',
					'header'           => $headers,
					'content'          => $post_content,
					'protocol_version' => 1.0,
				),
			)
		);

		return file_get_contents( $url, false, $context );
	}

	/**
	 * Composes a header string for HTTP requests
	 *
	 * @param array $headers Headers (Associative array - Name => Content).
	 *
	 * @return string Header string
	 */
	private static function header_string( array $headers ) {
		$result = '';
		foreach ( $headers as $name => $content ) {
			$result .= $name . ': ' . $content . "\r\n";
		}
		return $result;
	}
}
