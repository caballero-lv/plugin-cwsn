<?php
/**
 * Integration Demo Integration.
 *
 * @link       https://www.caballero.lv/
 * @since      1.0.0
 *
 * @package    Cwsn
 * @subpackage Cwsn/integration
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Cwsn
 * @subpackage Cwsn/integration
 * @author     Caballero <dev@caballero.lv>
 */
class Cwsn_Integration extends WC_Integration {

	/**
	 * Init and hook in the integration.
	 */
	public function __construct() {
		global $woocommerce;

		$this->id                 = 'cswn';
		$this->method_title       = __( 'SMS notifications', 'cwsn' );
		$this->method_description = __( 'Adds SMS notification when order completes', 'cwsn' );

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

		// Define user set variables.
		$this->enabled = $this->get_option( 'enabled' );
		$this->api_key = $this->get_option( 'api_key' );
		$this->url     = $this->get_option( 'url' );
		$this->sender  = $this->get_option( 'sender' );
		$this->message = $this->get_option( 'message' );

		// Actions.
		add_action( 'woocommerce_update_options_integration_' . $this->id, array( $this, 'process_admin_options' ) );

		// Filters.
		add_filter( 'woocommerce_settings_api_sanitized_fields_' . $this->id, array( $this, 'sanitize_settings' ) );
	}


	/**
	 * Initialize integration settings form fields.
	 *
	 * @return void
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'enabled' => array(
				'title'    => __( 'Enable', 'cwsn' ),
				'type'     => 'checkbox',
				'desc_tip' => false,
				'default'  => '',
			),
			'api_key' => array(
				'title'       => __( 'API Key', 'cwsn' ),
				'type'        => 'text',
				'description' => __( 'Enter your API Key.', 'cwsn' ),
				'desc_tip'    => true,
				'default'     => '',
			),
			'url'     => array(
				'title'       => __( 'URL', 'cwsn' ),
				'type'        => 'text',
				'description' => __( 'To what URL requests are made.', 'cwsn' ),
				'desc_tip'    => true,
				'default'     => '',
			),
			'sender'  => array(
				'title'       => __( 'Sender', 'cwsn' ),
				'type'        => 'text',
				'description' => __( 'Enter sender name.', 'cwsn' ),
				'desc_tip'    => true,
				'default'     => '',
			),
			'message' => array(
				'title'       => __( 'Message', 'cwsn' ),
				'type'        => 'textarea',
				'description' => __( 'Enter message to send. Available placeholders {order_number}', 'cwsn' ),
				'desc_tip'    => true,
				'default'     => '',
			),
		);
	}


	/**
	 * Santize our settings
	 *
	 * @see process_admin_options()
	 * @param array $settings Settings.
	 */
	public function sanitize_settings( $settings ) {
		return $settings;
	}


	/**
	 * Display errors by overriding the display_errors() method
	 *
	 * @see display_errors()
	 */
	public function display_errors() {

		// loop through each error and display it.
		foreach ( $this->errors as $key => $value ) :
			// translators: Error field.
			$error = sprintf( __( 'Looks like you made a mistake with the %s field.', 'cwsn' ), $value );
			?>
			<div class="error">
				<p><?php echo wp_kses_post( $error ); ?></p>
			</div>
			<?php
		endforeach;
	}
}
