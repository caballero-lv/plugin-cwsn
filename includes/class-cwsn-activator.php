<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.caballero.lv/
 * @since      1.0.0
 *
 * @package    Cwsn
 * @subpackage Cwsn/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Cwsn
 * @subpackage Cwsn/includes
 * @author     Caballero <dev@caballero.lv>
 */
class Cwsn_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
